# AUTHENTICATION MODULE


Projects that contains google authentication and module rerouting functions

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### **Prerequisites**
You need to have `node js`, min. version `12.14`, `npm` and `docker` installed in order to use this project. You also need your credentials from google in order to run the application on your machine, obtainable [here](https://developers.google.com/identity/protocols/OAuth2WebServer).

To get `node js` and `npm`, download it from [here](https://nodejs.org/dist/v12.14.1/node-v12.14.1-x64.msi)

To get and configure `docker`, download it from [here](https://docs.docker.com/docker-for-windows/install/) and follow [their start-up guide](https://docs.docker.com/get-started/).

### **Installing**
To build and run the application without creating a docker container you need to:

Get all the required node packages using
```
npm install
```

Run the application using
```
npm run dev
```
or 
```
npm run start
```
if you don't want `nodemon` to run.

If you want to containerize the aplication, you need to build a docker image with
```
docker build -t [image name] .
```
and run it using
```
docker run -d -p 8000:8000 [image name] --name [container name]
```

### **API Example**

Navigate to `http://localhost:8000` and login with your Google account. If you will see your session `uuid`, access token and email in `JSON` format, everything went as expected.