const app = require('express')()
const dotenv = require('dotenv')
const authData = require('./credentials.json')
const mongo = require('mongodb').MongoClient
const cookie = require('cookie-parser')
const uuid = require('uuid')
const axios = require('axios')
const {google} = require('googleapis')
const client_id = authData.client_id
const client_secret = authData.client_secret
const redirect_uris = authData.redirect_uris
const authClient = new google.auth.OAuth2(client_id,client_secret,redirect_uris)
dotenv.config()

let authed = false

app.use(cookie())

app.get('/',(req,res) => 
{
    if (!authed)
    {
        res.clearCookie('token')
        res.clearCookie('email')
        res.clearCookie('id')

        const url = authClient.generateAuthUrl({
            access_type:'offline',
            scope:[
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/gmail.readonly',
                'https://www.googleapis.com/auth/plus.login'
            ],
            prompt: 'select_account'
        })
        res.redirect(url)
    }
    else
    {
        writeToken(req.cookies['token'],req.cookies['id'],req.cookies['email'])
        res.clearCookie('token')
        res.clearCookie('email')
        res.send(req.cookies)
    }
})

app.get('/auth/google/callback',(req,res) => 
{
    const code = req.query.code
    if (code)
    {
        authClient.getToken(code,
            (err,tokens) => {
                if (err)
                {
                    throw err
                }
                else
                {
                    authClient.setCredentials(tokens)
                    authed = true
                    let id = uuid()
                    res.cookie("id",id,{maxAge: 600000}) 
                    res.cookie("token",tokens['access_token'],{maxAge: 600000})
                    res.redirect('/userinfo')
                }
        })
    }
})

app.get('/userinfo',(req,res) =>
{
    axios.get(`https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=${req.cookies['token']}`)
    .then(response => {
        res.cookie("email",response['data']['email'])
        res.redirect('/')
    })
})

app.get('/mails/:q?',(req,res) => 
{
    axios.get(`http://auth:8000/db/get/${req.cookies['id']}`)
    .then(response => {
        axios.get(`http://mail:9000/api/gmail/messages/all/${req.cookies['id']}/${req.params.q}`)
        .then(response => {
            res.json(response['data'])
        })
    })
})

const writeToken = (token,id,email) => 
{
    mongo.connect('mongodb://mongo:27017',(err,db) => 
    {
        if (err) throw err
        let dbo = db.db("Users")
        dbo.collection("Users").insertOne({id:id, token:token, email:email})
        db.close()
    })
}

app.get('/db/get/:id',(req,res) => 
{
    mongo.connect('mongodb://mongo:27017',(err,db) => 
    {
        if (err) throw err
        let dbo = db.db("Users")
        dbo.collection("Users").findOne({id: req.params.id},{projection: {_id:0, token:1, email: 1}},
            (err,result) =>
            {
                res.send(result)
            })
        db.close()
    })
})


const port = process.env.PORT || 9000
app.listen(port, () => console.log(`Server started on port ${port}`))