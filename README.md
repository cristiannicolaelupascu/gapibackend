# GoogleApi backend


Project that contains gmail api calls

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### **Prerequisites**
You need to have `docker` and `docker-compose` installed in order to use this project. You also need your credentials from google in order to run the application on your machine, obtainable [here](https://developers.google.com/identity/protocols/OAuth2WebServer). After obtaining them you need to rename the file to `credentials.json` and put it in `./auth-service`.

To get and configure `docker`, download it from [here](https://docs.docker.com/docker-for-windows/install/) and follow their start-up guide for [docker](https://docs.docker.com/get-started/) and for [compose](https://docs.docker.com/compose/gettingstarted/)


### **Installing**
To run the application, you need to build the docker images by using
```
docker-compose up 
```
If you want `compose` to run in the background, just add the `-d` flag.

To rebuild the containers, run
```
docker-compse build auth mail mongo
```

See their status with
```
docker-compose ps
```

### **API Example**

Navigate to `http://localhost:8000` and login with your Google account, then go to `http://localhost:8000/mails/[query]` to fetch your emails. More about gmail queries [here](https://support.google.com/mail/answer/7190?hl=en). If a `JSON` format document is returned, everything went as expected.
