const app = require('express')()
const axios = require('axios')
const cookie = require('cookie-parser')
const mongo = require('mongodb').MongoClient
app.use(cookie())


app.get('/api/gmail/messages/list/:q?',(req,res) =>
{
    axios.get(`https://www.googleapis.com/gmail/v1/users/${req.cookies['email']}/messages?access_token=${req.cookies['token']}&q=${req.params.q}`)
    .then(response => {
        res.json(response['data']['messages'].map(e => e.id))
    }).catch(err => {res.json(err.response['data']['error'])})
})

app.get('/api/gmail/messages/get/:id/:q?',(req,res) => 
{
    axios.get(`https://www.googleapis.com/gmail/v1/users/${req.cookies['email']}/messages/${req.params.id}?access_token=${req.cookies['token']}&q=${req.params.q}`)
    .then(response => {
        res.json(response['data'])
    }).catch(err => res.json(err.response['data']['error']))
})

app.get('/api/gmail/messages/all/:user/:q',(req,res) => 
{
    axios.get(`http://mail:9000/db/get/${req.params.user}`)
    .then(response => 
        {
            axios.get(`https://www.googleapis.com/gmail/v1/users/${response['data']['email']}/messages?access_token=${response['data']['token']}&q=${req.params.q}`)
            .then(async r => {
                let a = await r['data']['messages']
                let ids = await a.map(e => e.id)
                const array = await Promise.all(ids.map(async id => {
                    const mails = await axios.get(`https://www.googleapis.com/gmail/v1/users/${response['data']['email']}/messages/${id}?access_token=${response['data']['token']}&q=${req.params.q}`)
                    .then(response => {
                        return response
                    })
                    const mail = await mails['data']
                    return mail
                } ))
                res.json(array)
            })
        })
})

app.get('/db/get/:id',(req,res) => 
{
    mongo.connect('mongodb://mongo:27017',(err,db) => 
    {
        if (err) throw err
        let dbo = db.db("Users")
        dbo.collection("Users").findOne({id: req.params.id},{projection: {_id:0, token:1, email: 1}},
            (err,result) =>
            {
                res.send(result)
            })
        db.close()
    })
})

const port = 9000
app.listen(port, () => console.log(`Server started on port ${port}`))