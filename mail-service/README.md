# GMAIL MODULE


Project that contains gmail api calls

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### **Prerequisites**
You need to have `node js`, min. version `12.14`, `npm` and `docker` installed in order to use this project.

To get `node js` and `npm`, download it from [here](https://nodejs.org/dist/v12.14.1/node-v12.14.1-x64.msi)

To get and configure `docker`, download it from [here](https://docs.docker.com/docker-for-windows/install/) and follow [their start-up guide](https://docs.docker.com/get-started/).

### **Installing**
To build and run the application without creating a docker container you need to:

Get all the required node packages using
```
npm install
```

Run the application using
```
npm run dev
```
or 
```
npm run start
```
if you don't want `nodemon` to run.

If you want to containerize the aplication, you need to build a docker image with
```
docker build -t [image name] .
```
and run it using
```
docker run -d -p 9000:9000 [image name] --name [container name]
```

### **API Example**

Navigate to `http://localhost:8000` and login with your Google account, then go to `http://localhost:8000/mails/[query]` to fetch your emails. More about gmail queries [here](https://support.google.com/mail/answer/7190?hl=en). If a `JSON` format document is returned, everything went as expected.